<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Images extends Controller{

    //Load View
    public function action_index(){
        $view = View::factory('index');
      $this->response->body($view);
        }



    //Upload the image and create random string for filename
    protected function _upload_image($image,$ext){
        //Validation
        if(
            !Upload::valid($image) OR
            !Upload::not_empty($image) OR
            !Upload::type($image,['jpg','png','jpeg','gif'])
        ){
            return false;
        }

        $dir = DOCROOT.'uploads/';
        if($file = Upload::save($image,null,$dir)){
            $filename = strtolower(Text::random('alnum','20').'.'.$ext);
            Image::factory($file)
                ->resize(500,300,Image::AUTO)
                ->save($dir.$filename);
                unlink($file);
                return $filename;
        }
    }


    //API Methods

   //create a record
   public function action_uploadImage(){
    $data = $this->request->post();
    if($data){
        $filename = $_FILES['image_uploaded']['name'];
        $ext  = explode('.',$filename);
        $ext = end($ext);
        $filename = $this->_upload_image($_FILES['image_uploaded'],$ext);
        $data['image_uploaded'] = $filename;

        $image = ORM::factory('Image');
        $image->image_title = $data['image_title'];
        $image->image_url = "/uploads/".$data['image_uploaded'];
        $image->save();
    }else{
        return $this->response->body("Image not saved!");
    }
    return $this->response->body(json_encode('Image Saved!'));
}

    //Get all records
    public function action_all(){
        $images = ORM::factory('Image')->getAll();
        $images = json_encode($images);
        return $this->response->body($images);
    }

    //Get a record
    public function action_show(){
        $id = $this->request->param()['id'];
        $image = ORM::factory("Image",$id)->as_array();
        $image = json_encode($image);
        return $this->response->body($image);
        }

    //Update Record
    public function action_update(){
        $data = $this->request->post();
        if($data){
            $id = $data['id'];
            $image = ORM::Factory("Image",$id);
            $image->image_title = $data['image_title'];
            $image->save();
            return $this->response->body(json_encode("Image Updated!"));
        }else{
            return $this->response->body(json_encode("Update Failed!"));
        }
    }

    //Delete Record
    public function action_delete(){
        try{     
            $id= $this->request->param()['id'];
            $image = ORM::Factory('Image',$id);
            $file = ".".$image->image_url;
            unlink($file);
            $image->delete();
            return $this->request->body(json_encode("Image Deleted!"));
        }catch(Error $e){
            return $this->request->body(json_encode($e->getMessage()));
        }

    }
}