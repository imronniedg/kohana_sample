<?php defined('SYSPATH') or die('No direct script access.');

class Model_Image extends ORM{
    protected $_table = "images";

    public function getAll(){
        $query = DB::select()
            ->order_by('created_at','desc')    
            ->from($this->_table);
        $results = $query->execute()->as_array();
        return $results;
    }
}