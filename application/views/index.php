<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Corporate Keys Test</title>
    <link rel="stylesheet" href="assets/css/app.css">
    
</head>
<body>
<div class="nav-bar">
    <ul>
        <li><a href="#">Image Uploading App</a></li>
    </ul>
</div>
<div class="container">



<div class="row">
    <button id="btnNew" class="float-right btn btnAdd">Add Record</button>
</div>
<div id="list">
</div>

    <!-- Modals -->
    <div id="add-modal" class="modal">
        <div class="modal-header">Add New Record
            <span class="close">&times;</span>
        </div>
        <div class="modal-body">
            <form id="submit_photo" action="/corporate_keys/images/create" method="post" enctype="multipart/form-data">
                <label for="image_uploaded"> Select Image to Upload: </label>
                <input type="file" name="image_uploaded" id="image_uploaded" accept="image/x-png,image/gif,image/jpeg,image/jpg" required/><br>
                <label for="title">Title</label>
                <input type="text" name="image_title" id="title" required><br>
                <input type="submit" class="btn" value="Upload Image"/>
        </form>
        </div>
    </div>

    <div id="edit-modal" class="modal">
        <div class="modal-header">Edit Record
                <span class="close">&times;</span>
            </div>
        <div class="modal-body">
        <form id="edit_form">
                <label for="image_uploaded"> Uploaded Image: </label>
                <img src="" id="image_preview" />
                <br>
                <br>
                <br>
                <label for="title">Title</label>
                <input type="text" name="image_title" id="title"><br>
                <input type="hidden" name="id">
                <input type="submit" class="btn" value="Save"/>
        </form>
        </div>
    </div>

    <div class="modal" id="delete-modal">
        <div class="modal-header">Delete Record
            <span class="close">&times;</span>
        </div>
        <div class="modal-body">
        <p>
            Are you sure you want to delete this record?
        </p>
            <div class="buttons">
                <button class="btn" id="btnYes">Yes</button>
                <button class="btn" id="btnNo">No</button>
            </div>
        </div>
    </div>

</div>

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="assets/js/app.js"></script>
</body>
</html>