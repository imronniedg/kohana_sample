<?php defined('SYSPATH') or die('No direct script access.');

Route::set('delete_record','image/delete/<id>')
->defaults([
    "controller"=>"images",
    "action"=>"delete"
]);

Route::set('update_record','image/edit')
		->defaults([
			"controller"=>"images",
			"action"=>"update"
		]);

Route::set('create_record','images/create')
		->defaults([
			"controller"=>"images",
			"action"=>"uploadImage"
		]);

Route::set('view_record', 'images/show/<id>')
	->defaults([
		'controller' => 'images',
		'action'     => 'show',
	]);

Route::set('all_images', 'images/all')
	->defaults([
		'controller' => 'images',
		'action'     => 'all',
	]);