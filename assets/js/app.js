$(function(){

loadList();

function loadList(){
        let listDiv = $("#list");
        listDiv.html('');
        let header = "<tr>"+
                  "<th>Actions</th>"+
                    "<th>ID</th>"+
                    "<th>Title</th>"+
                    "<th>Thumbnail</th>"+
                    "<th>Date Added</th>"+
                    "</tr>";
        let rows="";
    
        $.ajax({
            url:"images/all",
            dataType:"json",
            success:(data)=>{
                if(data.length > 0){
                    data.map((record)=>{
                        rows+="<tr>"+
                                "<td><button class='btn btnEdit' id='"+record.id+"'>Edit</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btnDelete' id='"+record.id+"'>Delete</button></td>"+
                                "<td>"+record.id+"</td>"+
                                "<td>"+record.image_title+"</td>"+
                                "<td>"+"<img src='/corporate_keys"+record.image_url+"' height='50' width='100'/></td>"+
                                "<td>"+record.created_at+"</td>";
                    });
                    var table = "<table>"+
                    "<thead>"+
                    header
                    +"</thead>"+
                    "<tbody>"+rows+"</tbody>"+
                    "</table>";
                    listDiv.append(table);
                }else{
                    rows="<tr><td style='text-align:center;padding:50px;' colspan='5'>No Records</td></tr>";
                    var table = "<table>"+
                    "<thead>"+
                    header
                    +"</thead>"+
                    "<tbody>"+rows+"</tbody>"+
                    "</table>";
                    listDiv.append(table);
                }
                }
        })
};

$("#submit_photo").on('submit',(evt)=>{
    evt.preventDefault();
        var formData = new FormData();
        formData.set('image_title',$("#submit_photo>input[name=image_title]").val());
        formData.set('image_uploaded',$("#submit_photo>input[type=file]")[0].files[0]);

        console.log(this);
        let file = $("input[name=image_uploaded]").val();
        let title = $("input[name=image_title]").val();

        if(file != "" && title != ""){

            $.ajax({
                url:"/corporate_keys/images/create",
                type:'POST',
                dataType:'json',
                data:formData,
                contentType: false ,
                cache:false,
                processData: false,
                success:(data)=>{
                    alert('success');
                    loadList();
                    $('#add-modal').fadeOut(300);
                }
            });
        }
});


$("#btnNew").click(()=>{
    $('#add-modal').fadeIn(300);
    $(".close").click(()=>{
        $('#add-modal').fadeOut(300);
    });
});

//Click Handler Edit Modal
$("body").on('click','.btnEdit',(e)=>{
    //load the data
    let id = e.target.id;
    $.ajax({
        url:"/corporate_keys/images/show/"+id,
        dataType:"json",
        success:(data)=>{
            let url = "/corporate_keys"+data.image_url;
            $("#edit-modal img").attr('src',url);
            $("#edit-modal input[name=image_title]").attr('value',data.image_title);
            $("#edit-modal input[name=id]").attr('value',data.id);
        }
    })

    //display the modal
    $('#edit-modal').fadeIn(300);

    $(".close").click(()=>{
        $('#edit-modal').fadeOut(300);
        $("#edit-modal img").attr('src',"");
            $("#edit-modal input[name=image_title]").attr('value',"");
    });
});

$("body").on('click','.btnDelete',(evt)=>{
    let id = evt.target.id;
    $('#delete-modal').fadeIn(300);
    $(".close").click(()=>{
        $('#delete-modal').fadeOut(300);
    });

    $("#btnNo").click(()=>{
        $('#delete-modal').fadeOut(300);
    });

    $("#btnYes").click(()=>{
        $.ajax({
            method:'post',
            url:"/corporate_keys/image/delete/"+id,
            success:()=>{
                loadList();
                $('#delete-modal').fadeOut(300);
            }
        })
    });
});


$("#edit_form").submit((evt)=>{
    evt.preventDefault();
    let data = {
        id:$("#edit_form input[name=id]").val(),
        image_title:$("#edit_form input[name=image_title]").val(),
    }
    $.ajax({
        url:"/corporate_keys/image/edit",
        method:"post",
        data:data,
        success:(data)=>{
            alert(data);
            loadList();
            $('#edit-modal').fadeOut(300);
        }
    });
});

});